<img src="./public/logo-pokegrowth.png" width="80%" style="margin-bottom: 20px;" />


# Table of Content

1. [What is Pokegrowth?](#what-is-pokegrowth)
2. [Dependencies](#dependencies)
3. [Quick Start](#quick-start)


## What is Pokegrowth?
Pokegrowth is a Pokemon app where you can choose one Pokemon to be your pet. You'll take care of your chosen Pokemon by feeding it and helping it evolve.

1. Choose Your Pokemon: Select one Pokemon to be your partner and take care of it.

2. Feeding Mechanism: You need to feed your Pokemon regularly with the all berries you can choose one of them. Each berry have the firmness information to gain your pokemon weight.

3. Weight Management: Be mindful of the firmness of the berry you feed your Pokemon. If you feed your Pokemon with the same firmness as the previous meal, it will lose your pokemon weight.


## Dependencies
- ⚡️ Next.js 13
- ⚛️ React 18
- ⛑ TypeScript
- ✨ Styled Components - CssInJs for component styling
- 📏 ESLint — To find and fix problems in your code
- 💖 Prettier — Code Formatter for consistent style
- 🐶 Husky — For running scripts before committing
- 🚓 Commitlint — To make sure your commit messages follow the convention
- 🚫 lint-staged — Run ESLint and Prettier against staged Git files
- 🗂 Path Mapping — Import components or images using the `~/` prefix


## Quick Start

Clone this repository using ssh or https, and then:

```
# yarn
yarn install

# npm
npx install
```

### Development

To start the project locally, run:

```
  # yarn
  yarn dev

  # npm
  npm run dev
```

Open `http://localhost:3000` with your browser to see the result.


### Requirements

- Node.js >= 16
- npm or yarn


### Directory Structure

- [`.husky`](.husky) — Husky configuration and hooks.<br>
- [`public`](./public) — Static assets such as robots.txt, images, and favicon.<br>
- [`src`](./src) — Application source code, including pages, components, styles.<br>


### Scripts
- `yarn/npm dev` — Starts the application in development mode at `http://localhost:3000`.
- `yarn/npm build` — Creates an optimized production build of your application.
- `yarn/npm start` — Starts the application in production mode.
- `yarn/npm lint` — Runs ESLint for all files in the `src` directory.


### Path Mapping

TypeScript are pre-configured with custom path mappings. To import components or files, use the `~/` prefix.

```tsx
import { Button } from '~/presentation/components/Button';

// To import images or other files from the public folder
import avatar from '~/public/avatar.png';
```