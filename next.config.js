/** @type {import('next').NextConfig} */

const IMAGE_HOST_DOMAINS = [`raw.githubusercontent.com`];

const cspHeader = `
  upgrade-insecure-requests; object-src 'none'; block-all-mixed-content;
  frame-ancestors 'self' pokegrowth.vercel.app *.vercel.app;
  require-trusted-types-for 'script';
`;

const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: IMAGE_HOST_DOMAINS,
  },
  async headers() {
    return [
      {
        source: "/(.*)",
        headers: [
          {
            key: "X-Frame-Options",
            value: "DENY",
          },
          {
            key: "X-Content-Type-Options",
            value: "nosniff",
          },
          {
            key: "X-XXS-Protection",
            value: "1; mode=block",
          },
          {
            key: "Content-Security-Policy",
            value: cspHeader.replace(/\n/g, ''),
          },
        ],
      },
    ];
  },
};

module.exports = nextConfig;
