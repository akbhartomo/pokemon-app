import { IEvolutionChainPokemon } from "~/domain/model";
import { IPokemonEvolutionChainResponse } from "../entity";

export function mapRemoteEvolutionChainPokemonToLocal(
  pokemonEntity: IPokemonEvolutionChainResponse
): IEvolutionChainPokemon {
  return {
    nextEvolId: +pokemonEntity.chain.evolves_to[0].species.url.split("/")[6],
    nextEvolName: pokemonEntity.chain.evolves_to[0].species.name,
  };
}
