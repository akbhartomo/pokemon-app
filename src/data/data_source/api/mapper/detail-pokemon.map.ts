import { IPokemonDetailResponse } from "../entity";
import { IDetailPokemonModel } from "~/domain/model";
import { BASE_POKEMON_OFFICIAL_ARTWORK } from "~/shared/constants/base-url.constant";

export function mapRemotePokemonDetailToLocal(
  pokemonEntity: IPokemonDetailResponse
): IDetailPokemonModel {
  const id = pokemonEntity.id;
  return {
    id: pokemonEntity.id,
    name: pokemonEntity.name,
    image: `${BASE_POKEMON_OFFICIAL_ARTWORK}/${id}.png`,
    stats: pokemonEntity.stats
      .map((item) => ({
        base_stat: item.base_stat,
        stat: {
          name: item.stat.name,
        },
      }))
      .filter(
        (key) =>
          key.stat.name !== "special-attack" &&
          key.stat.name !== "special-defense"
      ),
    weight: pokemonEntity.weight,
    nextEvolution: pokemonEntity.nextEvolution || "",
    maxWeight: pokemonEntity.maxWeight,
    prevMeal: pokemonEntity.prevMeal,
  };
}
