import { IDetailSpeciesPokemon } from "~/domain/model";
import { IDetailPokemonSpeciesResponse } from "../entity";

export function mapRemoteDetailSpeciesPokemonToLocal(
  pokemonEntity: IDetailPokemonSpeciesResponse
): IDetailSpeciesPokemon {
  return {
    evolId: +pokemonEntity.evolution_chain?.url.split("/")[6],
  };
}
