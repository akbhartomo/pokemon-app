import { IListBerriesResponse } from "../entity";
import { IListBerry } from "~/domain/model/berry/list-berries.model";
import { BASE_BERRY_OFFICIAL_ARTWORK } from "~/shared/constants/base-url.constant";

export function mapRemoteListBerriesToLocal(
  berryEntity: IListBerriesResponse
): IListBerry {
  return {
    results: berryEntity.results.map((item) => {
      const id = item.url.split("/")[6];
      const name = item.name;
      return {
        name: name,
        id: +id,
        image: `${BASE_BERRY_OFFICIAL_ARTWORK}/${name}-berry.png`,
      };
    }),
  };
}
