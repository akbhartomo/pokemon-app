import { IPokemonEvolutionChainResponse } from "../entity";

export function mapRemoteEvolutionDetailToLocal(
  res: IPokemonEvolutionChainResponse,
  newData: any,
  evolveFrom?: boolean
) {
  let newName = "";

  if (newData.length === 0) {
    newName = res.chain.species.name;
    return newName;
  }

  if (newData.length === 1) {
    newName = newData[0].name;
    return newName;
  }

  if (newData.length > 0 && !evolveFrom) {
    newName = newData[0].name;
    return newName;
  }

  if (newData.length > 0 && evolveFrom) {
    newName = newData[1].name;
    return newName;
  }

  return newName;
}
