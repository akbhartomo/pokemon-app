import { IPokemonResponse } from "../entity";
import { IListPokemon } from "~/domain/model";
import { BASE_POKEMON_OFFICIAL_ARTWORK } from "~/shared/constants/base-url.constant";

export function mapRemoteListPokemonsToLocal(
  pokemonEntity: IPokemonResponse
): IListPokemon {
  return {
    results: pokemonEntity.results.map((item) => {
      const id = item.url.split("/")[6];
      return {
        id: id,
        image: `${BASE_POKEMON_OFFICIAL_ARTWORK}/${id}.png`,
        name: item.name,
        url: item.url,
      };
    }),
  };
}
