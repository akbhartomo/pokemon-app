import { IDetailBerryModel } from "~/domain/model";
import { IDetailBerry } from "../entity";

export function mapRemoteBerryDetailToLocal(
  berryEntity: IDetailBerry
): IDetailBerryModel {
  return {
    firmness: berryEntity.firmness.name,
  };
}
