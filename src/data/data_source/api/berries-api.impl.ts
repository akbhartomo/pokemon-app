import { CallApiParams, IDetailBerryModel } from "~/domain/model";
import { callApi } from "~/shared/configs/fetcher";
import localStorageService from "~/shared/utils/local-storage.adapter";
import { BerriesDataSource } from "../berries-data.source";
import { mapRemoteBerryDetailToLocal } from "./mapper/detail-berry.map";

export function BerriesApiImplementation(): BerriesDataSource {
  const getListBerriesImpl = async (limit: number) => {
    const options: CallApiParams = {
      method: "GET",
      uri: `api/v2/berry?limit=${limit}`,
    };
    const { data } = await callApi(options);
    return data;
  };

  const getDetailBerryImpl = async (id: string | number) => {
    const options: CallApiParams = {
      method: "GET",
      uri: `api/v2/berry/${id}`,
    };
    const { data } = await callApi(options);
    return data;
  };

  const handleChooseBerry = async (param: number) => {
    const localdb = localStorageService<{ name: string }>("berry", "secret");
    const res = await getDetailBerryImpl(param);
    const data = mapRemoteBerryDetailToLocal(res);

    localdb.insert<IDetailBerryModel>(data);

    return data;
  };

  return {
    getListBerriesImpl,
    getDetailBerryImpl,
    handleChooseBerry,
  };
}
