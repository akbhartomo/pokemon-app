import { callApi } from "~/shared/configs/fetcher";
import { CallApiParams } from "~/domain/model";
import { PokemonDataSource } from "../pokemon-data.source";
import {
  HARD,
  SOFT,
  SUPER_HARD,
  VERY_HARD,
  VERY_SOFT,
} from "~/shared/constants/firmness.constant";
import localStorageService from "~/shared/utils/local-storage.adapter";
import { ensureNonNegative } from "~/shared/utils/non-negative";
import { mapRemotePokemonDetailToLocal } from "./mapper/detail-pokemon.map";
import {
  IDetailPokemonSpeciesResponse,
  IDetailPokemonEntity,
  IDetailBerryEntity,
} from "./entity";
import { mapRemoteDetailSpeciesPokemonToLocal } from "./mapper/species-pokemon.map";
import { mapRemoteEvolutionDetailToLocal } from "./mapper/evolution-data.map";

export function mergeEvolutions(evolutions: any[]): any[] {
  return evolutions.reduce((acc, curr) => {
    acc.push(curr.species);

    if (curr.evolves_to.length > 0) {
      acc.push(...mergeEvolutions(curr.evolves_to));
    }

    return acc;
  }, [] as any[]);
}

export function getIncreaseBy(
  firmness: string,
  isPoison: boolean,
  weight: number
) {
  let increaseBy = 0;

  switch (firmness) {
    case VERY_SOFT:
      increaseBy = 2;
      break;
    case SOFT:
      increaseBy = 3;
      break;
    case HARD:
      increaseBy = 5;
      break;
    case VERY_HARD:
      increaseBy = 8;
      break;
    case SUPER_HARD:
      increaseBy = 10;
      break;
    default:
      increaseBy = 1;
      break;
  }

  return isPoison
    ? ensureNonNegative(weight + increaseBy * -2)
    : increaseBy + weight;
}

export function PokemonApiImplementation(): PokemonDataSource {
  const getListPokemonImpl = async (offset: number, limit: number) => {
    const options: CallApiParams = {
      method: "GET",
      uri: `api/v2/pokemon?offset=${offset}&limit=${limit}`,
    };
    const { data } = await callApi(options);
    return data;
  };

  const getDetailPokemonImpl = async (param: string | number) => {
    const options: CallApiParams = {
      method: "GET",
      uri: `api/v2/pokemon/${param}`,
    };
    const { data } = await callApi(options);
    return data;
  };

  const getSpeciesPokemonImpl = async (param: string | number) => {
    const options: CallApiParams = {
      method: "GET",
      uri: `api/v2/pokemon-species/${param}`,
    };
    const res = await callApi(options);

    if (!res) {
      return;
    }
    return res.data;
  };

  const getEvolutionChainPokemonImpl = async (id: number) => {
    const options: CallApiParams = {
      method: "GET",
      uri: `api/v2/evolution-chain/${id}`,
    };
    const { data } = await callApi(options);
    return data;
  };

  const getProvideEvolutionPokemonImpl = async (
    id: number,
    evolveFrom?: boolean
  ) => {
    const res = await getEvolutionChainPokemonImpl(id);
    const merge = mergeEvolutions(res.chain.evolves_to);
    const data = mapRemoteEvolutionDetailToLocal(res, merge, evolveFrom);

    return data;
  };

  const getSpeciesToEvolutionChain = async (
    data: IDetailPokemonSpeciesResponse,
    evolveFrom?: boolean
  ) => {
    const species = mapRemoteDetailSpeciesPokemonToLocal(data);
    const name = await getProvideEvolutionPokemonImpl(
      species.evolId,
      evolveFrom
    );

    return name;
  };

  const getAllDetailPokemonDataImpl = async (param: string | number) => {
    const localdb = localStorageService<IDetailPokemonEntity>(
      "pokemon",
      "secret"
    );

    const results = await Promise.all([
      getDetailPokemonImpl(param),
      getSpeciesPokemonImpl(param),
    ]);
    const resFilter = results.filter((data) => data !== undefined);
    const datas = Object.assign({}, ...resFilter);

    const isEvolveFrom = !!datas.evolves_from_species?.name;

    const evolutionName = await getSpeciesToEvolutionChain(datas, isEvolveFrom);
    const { weight } = await getDetailPokemonImpl(evolutionName);

    const newData = {
      ...datas,
      nextEvolution: evolutionName,
      maxWeight: weight,
      prevMeal: "",
    };

    const data = mapRemotePokemonDetailToLocal({ ...newData });
    if (param) localdb.insert<IDetailPokemonEntity>(data);

    return data;
  };

  const handleFeedPokemon = () => {
    const db_berry = localStorageService<{ name: string }>("berry", "secret");
    const db_pokemon = localStorageService<IDetailPokemonEntity>(
      "pokemon",
      "secret"
    );
    const { firmness } = db_berry.getAll<IDetailBerryEntity>();
    const dataPokemon = db_pokemon.getAll<IDetailPokemonEntity>();

    const weight = dataPokemon.weight;
    const isPoison = dataPokemon.prevMeal === firmness;

    dataPokemon.weight = getIncreaseBy(firmness, isPoison, weight);
    dataPokemon.prevMeal = firmness;

    db_pokemon.update("pokemon", dataPokemon);

    return dataPokemon;
  };

  const handleEvolvePokemon = (newPokemon: IDetailPokemonEntity) => {
    const localdb = localStorageService<IDetailPokemonEntity>(
      "pokemon",
      "secret"
    );

    localdb.update("pokemon", newPokemon);
    const dataPokemon = localdb.getAll<IDetailPokemonEntity>();

    return dataPokemon;
  };

  return {
    getListPokemonImpl,
    getDetailPokemonImpl,
    getSpeciesPokemonImpl,
    getEvolutionChainPokemonImpl,
    getAllDetailPokemonDataImpl,
    handleFeedPokemon,
    handleEvolvePokemon,
  };
}
