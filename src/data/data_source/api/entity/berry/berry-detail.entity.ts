export interface IDetailBerry {
  firmness: Firmness;
  flavors: Flavor[];
  growth_time: number;
  id: number;
  item: Firmness;
  max_harvest: number;
  name: string;
  natural_gift_power: number;
  natural_gift_type: Firmness;
  size: number;
  smoothness: number;
  soil_dryness: number;
}

export interface Firmness {
  name: string;
  url: string;
}

export interface Flavor {
  flavor: Firmness;
  potency: number;
}

export interface IDetailBerryEntity {
  id: number;
  firmness: string;
}
