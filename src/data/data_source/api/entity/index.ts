export * from "./pokemon/pokemon-list.entity";
export * from "./pokemon/pokemon-detail.entity";
export * from "./pokemon/pokemon-species.entity";
export * from "./pokemon/pokemon-evolution-chain.entity";
export * from "./berry/berries-list.entity";
export * from "./berry/berry-detail.entity";
