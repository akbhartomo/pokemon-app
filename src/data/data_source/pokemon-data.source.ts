import type {
  IDetailPokemonEntity,
  IDetailPokemonSpeciesResponse,
  IPokemonDetailResponse,
  IPokemonEvolutionChainResponse,
  IPokemonResponse,
} from "./api/entity";

export interface PokemonDataSource {
  getListPokemonImpl(offset: number, limit: number): Promise<IPokemonResponse>;
  getDetailPokemonImpl(param: string | number): Promise<IPokemonDetailResponse>;
  getSpeciesPokemonImpl(
    param: string | number
  ): Promise<IDetailPokemonSpeciesResponse>;
  getEvolutionChainPokemonImpl(
    id: number
  ): Promise<IPokemonEvolutionChainResponse>;
  getAllDetailPokemonDataImpl(
    param: string | number
  ): Promise<IDetailPokemonEntity>;

  handleFeedPokemon(): IDetailPokemonEntity;
  handleEvolvePokemon(newPokemon: IDetailPokemonEntity): IDetailPokemonEntity;
}
