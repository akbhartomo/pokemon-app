import { IDetailBerryModel } from "~/domain/model";
import type { IDetailBerry, IListBerriesResponse } from "./api/entity";

export interface BerriesDataSource {
  getListBerriesImpl(limit: number): Promise<IListBerriesResponse>;
  getDetailBerryImpl(name: string): Promise<IDetailBerry>;

  handleChooseBerry(id: number): Promise<IDetailBerryModel>;
}
