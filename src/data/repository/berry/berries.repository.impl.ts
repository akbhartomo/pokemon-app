import { mapRemoteListBerriesToLocal } from "~/data/data_source/api/mapper/list-berries.map";
import { mapRemoteBerryDetailToLocal } from "~/data/data_source/api/mapper/detail-berry.map";
import { BerriesDataSource } from "~/data/data_source/berries-data.source";
import { BerriesRepositories } from "~/domain/repositories/berries.repos";

export function BerriesDataRepositoryImpl(
  dataSource: BerriesDataSource
): BerriesRepositories {
  const getListBerriesDataRepo = async (limit: number) => {
    const response = await dataSource.getListBerriesImpl(limit);
    return mapRemoteListBerriesToLocal(response);
  };

  const getDetailBerryDataRepo = async (name: string) => {
    const response = await dataSource.getDetailBerryImpl(name);
    return mapRemoteBerryDetailToLocal(response);
  };

  const handleChooseBerryDataRepo = async (id: number) => {
    const response = await dataSource.handleChooseBerry(id);
    return response;
  };

  return {
    getListBerriesDataRepo,
    getDetailBerryDataRepo,
    handleChooseBerryDataRepo,
  };
}
