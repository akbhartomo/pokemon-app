import { IDetailPokemonEntity } from "~/data/data_source/api/entity";
import { mapRemoteListPokemonsToLocal } from "~/data/data_source/api/mapper/list-pokemon.map";
import { mapRemotePokemonDetailToLocal } from "~/data/data_source/api/mapper/detail-pokemon.map";
import { mapRemoteDetailSpeciesPokemonToLocal } from "~/data/data_source/api/mapper/species-pokemon.map";
import { mapRemoteEvolutionChainPokemonToLocal } from "~/data/data_source/api/mapper/evolution-chain-pokemon.map";
import { PokemonDataSource } from "~/data/data_source/pokemon-data.source";
import { PokemonRepositoriesDomain } from "~/domain/repositories/pokemon.repos";

export function PokemonDataRepositoryImpl(
  dataSource: PokemonDataSource
): PokemonRepositoriesDomain {
  const getListPokemonDataRepo = async (offset: number, limit: number) => {
    const response = await dataSource.getListPokemonImpl(offset, limit);
    return mapRemoteListPokemonsToLocal(response);
  };

  const getDetailPokemonDataRepo = async (param: string | number) => {
    const response = await dataSource.getDetailPokemonImpl(param);
    return mapRemotePokemonDetailToLocal(response);
  };

  const getDetailPokemonSpeciesDataRepo = async (param: string | number) => {
    const response = await dataSource.getSpeciesPokemonImpl(param);
    return mapRemoteDetailSpeciesPokemonToLocal(response);
  };

  const getEvolutionChainPokemonDataRepo = async (id: number) => {
    const response = await dataSource.getEvolutionChainPokemonImpl(id);
    return mapRemoteEvolutionChainPokemonToLocal(response);
  };

  const getAllDetailPokemonDataRepo = async (param: number | string) => {
    const result = await dataSource.getAllDetailPokemonDataImpl(param);
    return result;
  };

  const handleFeedPokemonDataRepo = () => {
    const res = dataSource.handleFeedPokemon();
    return res;
  };

  const handleEvolvePokemonDataRepo = (newPokemon: IDetailPokemonEntity) => {
    const response = dataSource.handleEvolvePokemon(newPokemon);
    return response;
  };

  return {
    getListPokemonDataRepo,
    getDetailPokemonDataRepo,
    getDetailPokemonSpeciesDataRepo,
    getEvolutionChainPokemonDataRepo,
    getAllDetailPokemonDataRepo,
    handleFeedPokemonDataRepo,
    handleEvolvePokemonDataRepo,
  };
}
