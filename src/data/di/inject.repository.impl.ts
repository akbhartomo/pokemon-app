import { useMemo } from "react";
import { PokemonApiImplementation } from "../data_source/api/pokemon-api.impl";
import { PokemonDataRepositoryImpl } from "../repository";
import { BerriesApiImplementation } from "../data_source/api/berries-api.impl";
import { BerriesDataRepositoryImpl } from "../repository/berry/berries.repository.impl";

export function useInject() {
  const injectPokemonApi = useMemo(() => PokemonApiImplementation(), []);
  const injectBerriesApi = useMemo(() => BerriesApiImplementation(), []);

  const injectPokemonRepositoryImpl = useMemo(
    () => PokemonDataRepositoryImpl(injectPokemonApi),
    []
  );

  const injectBerriesRepositoryImpl = useMemo(
    () => BerriesDataRepositoryImpl(injectBerriesApi),
    []
  );

  return {
    injectPokemonRepositoryImpl,
    injectBerriesRepositoryImpl,
  };
}
