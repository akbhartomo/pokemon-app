import dynamic from "next/dynamic";
import Layout from "~/presentation/components/Layout";
import LayoutProvider from "~/shared/contexts/LayoutProvider";

const DynamicDetailTemplate = dynamic(
  () => import("~/presentation/pages/Detail"),
  {
    ssr: false,
  }
);

export default async function Page() {
  return (
    <LayoutProvider>
      <Layout>
        <DynamicDetailTemplate />
      </Layout>
    </LayoutProvider>
  );
}
