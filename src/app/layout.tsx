import React from "react";
import QueryProvider from "~/presentation/components/QueryClientProvider";
import { Providers } from "./provider";

interface IRootLayout {
  readonly children: React.ReactNode;
}

export default async function RootLayout({ children }: IRootLayout) {
  return (
    <html lang="en">
      <head>
        <title>Pokemon Growth</title>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="/images/apple-touch-icon.png" />
        <link
          rel="apple-touch-icon"
          sizes="57x57"
          href="/images/apple-touch-icon-57x57.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="/images/apple-touch-icon-72x72.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="76x76"
          href="/images/apple-touch-icon-76x76.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="114x114"
          href="/images/apple-touch-icon-114x114.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="120x120"
          href="/images/apple-touch-icon-120x120.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href="/images/apple-touch-icon-144x144.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="152x152"
          href="/images/apple-touch-icon-152x152.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/images/apple-touch-icon-180x180.png"
        />
        <meta
          name="description"
          content="A simple project of Pokemon Growth that you can pick a pokemon and get it more feed to get evolve"
        />
        <meta property="og:title" content="Pokemon Growth" />
        <meta
          property="og:description"
          content="A simple project of Pokemon Growth that you can pick a pokemon and get it more feed to get evolve"
        />
        <meta property="og:image" content="/images/apple-touch-icon.png" />
        <meta name="twitter:title" content="Pokemon Growth" />
        <meta
          name="twitter:description"
          content="A simple project of Pokemon Growth that you can pick a pokemon and get it more feed to get evolve"
        />
        <meta name="twitter:image" content="/images/apple-touch-icon.png" />
        <meta name="twitter:card" content="/images/apple-touch-icon.png" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin=""
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Manrope:wght@200..800&display=swap"
          rel="stylesheet"
        />
      </head>
      <body suppressHydrationWarning={true}>
        <Providers>
          <QueryProvider>{children}</QueryProvider>
        </Providers>
      </body>
    </html>
  );
}
