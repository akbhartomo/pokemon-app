import { Flex } from "@chakra-ui/react";
import Layout from "~/presentation/components/Layout";
import Loader from "~/presentation/components/Loader";

export default function Loading() {
  return (
    <Layout>
      <Flex alignItems="center" justifyContent="center">
        <Loader />
      </Flex>
    </Layout>
  );
}
