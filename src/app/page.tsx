import Layout from "~/presentation/components/Layout";
import Search from "~/presentation/components/Search";
import HomeTemplate from "~/presentation/pages/Home";
import SearchProvider from "~/shared/contexts/SearchProvider";
import LayoutProvider from "~/shared/contexts/LayoutProvider";

export default async function Page() {
  return (
    <SearchProvider>
      <LayoutProvider>
        <Layout search={<Search />}>
          <HomeTemplate />
        </Layout>
      </LayoutProvider>
    </SearchProvider>
  );
}
