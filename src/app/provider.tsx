"use client";

import React from "react";
import { CacheProvider } from "@chakra-ui/next-js";
import { ChakraProvider } from "@chakra-ui/react";
import { createTheme } from "~/shared/styles/theme";

interface TProviders {
  readonly children: React.ReactNode;
}

export const Providers: React.FC<TProviders> = ({ children }) => {
  return (
    <CacheProvider>
      <ChakraProvider theme={createTheme}>{children}</ChakraProvider>
    </CacheProvider>
  );
};

export default Providers;
