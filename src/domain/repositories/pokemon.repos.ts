import {
  IDetailPokemonModel,
  IDetailSpeciesPokemon,
  IEvolutionChainPokemon,
  IListPokemon,
} from "../model";

export interface PokemonRepositoriesDomain {
  getListPokemonDataRepo(offset: number, limit: number): Promise<IListPokemon>;
  getDetailPokemonDataRepo(
    param: string | number
  ): Promise<IDetailPokemonModel>;
  getDetailPokemonSpeciesDataRepo(
    param: string | number
  ): Promise<IDetailSpeciesPokemon>;
  getEvolutionChainPokemonDataRepo(id: number): Promise<IEvolutionChainPokemon>;
  getAllDetailPokemonDataRepo(
    param: string | number
  ): Promise<IDetailPokemonModel>;

  handleFeedPokemonDataRepo(): IDetailPokemonModel;
  handleEvolvePokemonDataRepo(
    newPokemon: IDetailPokemonModel
  ): IDetailPokemonModel;
}
