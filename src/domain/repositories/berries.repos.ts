import type { IDetailBerryModel, IListBerry } from "../model";

export interface BerriesRepositories {
  getListBerriesDataRepo(limit: number): Promise<IListBerry>;
  getDetailBerryDataRepo(name: string): Promise<IDetailBerryModel>;

  handleChooseBerryDataRepo(id: number): Promise<IDetailBerryModel>;
}
