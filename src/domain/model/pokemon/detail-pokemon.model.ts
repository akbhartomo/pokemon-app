export interface IOfficialArtwork {
  front_default?: string;
}

export interface ISpritesOther {
  "official-artwork": IOfficialArtwork;
}

export interface IPokemonSprites {
  other?: ISpritesOther;
}

export interface IStat {
  base_stat: number;
  stat: Species;
}

export interface Species {
  name: string;
}

export interface IDetailPokemonModel {
  id: number;
  name: string;
  image: string;
  stats: IStat[];
  weight: number;
  nextEvolution: string;
  maxWeight: number;
  prevMeal: string;
}

export interface IDetailPokemonAllProps extends IDetailPokemonModel {
  evolId: number;
  nextEvolId: number;
  nextEvolName: string;
}
