export interface IListPokemonItem {
  id: string;
  image: string;
  name: string;
  url: string;
}

export interface IListPokemon {
  results: Array<IListPokemonItem>;
}
