export interface IEvolutionChainPokemon {
  nextEvolId: number;
  nextEvolName: string;
}
