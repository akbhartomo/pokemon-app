import { Method } from "axios";

export interface CallApiParams {
  method: Method;
  uri: string;
}
