export interface IListBerryItem {
  name: string;
  id: number;
  image: string;
}

export interface IListBerry {
  results: IListBerryItem[];
}
