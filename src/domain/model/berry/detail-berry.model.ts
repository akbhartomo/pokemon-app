export interface IDetailBerryModel {
  firmness: string;
}

export interface IDetailBerryDescription {
  firmness: string;
  point: number;
}
