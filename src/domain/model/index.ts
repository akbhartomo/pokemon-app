export * from "./pokemon/list-pokemon.model";
export * from "./pokemon/detail-pokemon.model";
export * from "./pokemon/detail-species-pokemon.model";
export * from "./pokemon/evolution-chain-pokemon.model";

export * from "./berry/list-berries.model";
export * from "./berry/detail-berry.model";

export * from "./common/call-api.model";
