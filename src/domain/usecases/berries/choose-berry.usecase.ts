import { BerriesRepositories } from "~/domain/repositories/berries.repos";

export function chooseBerryUseCase(repository: BerriesRepositories) {
  const invoke = (id: number) => {
    const data = repository.handleChooseBerryDataRepo(id);
    return data;
  };

  return { invoke };
}
