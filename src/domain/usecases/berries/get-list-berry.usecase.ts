import { BerriesRepositories } from "~/domain/repositories/berries.repos";

export function getListBerriesUseCase(repository: BerriesRepositories) {
  const invoke = async (limit: number) => {
    const response = await repository.getListBerriesDataRepo(limit);
    return response;
  };

  return { invoke };
}
