import { BerriesRepositories } from "~/domain/repositories/berries.repos";

export function getDetailBerryUseCase(repository: BerriesRepositories) {
  const invoke = async (name: string) => {
    const data = await repository.getDetailBerryDataRepo(name);
    return data;
  };

  return { invoke };
}
