import { PokemonRepositoriesDomain } from "~/domain/repositories/pokemon.repos";

export function getSpeciesPokemonUseCase(
  repository: PokemonRepositoriesDomain
) {
  const invoke = async (param: string | number) => {
    const response = await repository.getDetailPokemonSpeciesDataRepo(param);
    return response;
  };

  return { invoke };
}
