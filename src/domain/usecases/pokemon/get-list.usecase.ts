import { PokemonRepositoriesDomain } from "../../repositories/pokemon.repos";

export function getListPokemonUseCase(repository: PokemonRepositoriesDomain) {
  const invoke = async (offset: number, limit: number) => {
    const response = await repository.getListPokemonDataRepo(offset, limit);
    return response;
  };

  return { invoke };
}
