import { IDetailPokemonModel } from "~/domain/model";
import { PokemonRepositoriesDomain } from "~/domain/repositories/pokemon.repos";

export function handleEvolvePokemonUseCase(
  repository: PokemonRepositoriesDomain
) {
  const invoke = (newPokemon: IDetailPokemonModel) => {
    const data = repository.handleEvolvePokemonDataRepo(newPokemon);
    return data;
  };

  return { invoke };
}
