import { PokemonRepositoriesDomain } from "~/domain/repositories/pokemon.repos";

export function getEvolutionChainUseCase(
  repository: PokemonRepositoriesDomain
) {
  const invoke = async (id: number) => {
    const response = await repository.getEvolutionChainPokemonDataRepo(id);
    return response;
  };

  return { invoke };
}
