import { PokemonRepositoriesDomain } from "../../repositories/pokemon.repos";

export function getDetailPokemonUseCase(repository: PokemonRepositoriesDomain) {
  const invoke = async (param: string | number) => {
    const res = repository.getAllDetailPokemonDataRepo(param);
    return res;
  };

  return { invoke };
}
