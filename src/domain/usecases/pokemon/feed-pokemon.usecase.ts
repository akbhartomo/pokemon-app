import { PokemonRepositoriesDomain } from "~/domain/repositories/pokemon.repos";

export function handleFeedPokemonUseCase(
  repository: PokemonRepositoriesDomain
) {
  const invoke = () => {
    const data = repository.handleFeedPokemonDataRepo();
    return data;
  };

  return { invoke };
}
