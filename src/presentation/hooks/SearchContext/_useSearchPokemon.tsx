import { useState } from "react";
import useDebounce from "~/shared/utils/debounce";

const useSearchPokemon = () => {
  const [searchQuery, setSearchQuery] = useState<string>("");
  const debounceSearch = useDebounce(searchQuery, 700);

  const handleSearchOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value);
  };

  return {
    searchQuery,
    handleSearchOnChange,
    debounceSearch,
  };
};

export default useSearchPokemon;
