import { useRouter } from "next/navigation";
import { useInject } from "~/data/di";
import { getDetailPokemonUseCase } from "~/domain/usecases";
import useFetchAllPokemons from "./_useFetchAllPokemons";
import useSearchPokemon from "./_useSearchPokemon";

export default function useSearchViewModel() {
  const { allPokemons, state, realTimeSearch } = useFetchAllPokemons();
  const { searchQuery, debounceSearch, handleSearchOnChange } =
    useSearchPokemon();
  const router = useRouter();
  const { injectPokemonRepositoryImpl } = useInject();

  const { loading, searchResult } = state;

  const fetchDetailPokemonUseCase = async (param: string | number) => {
    const data = await getDetailPokemonUseCase(
      injectPokemonRepositoryImpl
    ).invoke(param);

    return data;
  };

  const onClickToChoosePokemon = async (name: string | number) => {
    try {
      const data = await fetchDetailPokemonUseCase(name);
      router.push(`/pokemon/${data.name}`);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  };

  return {
    allPokemons,
    loading,
    searchResult,
    realTimeSearch,
    searchQuery,
    debounceSearch,
    handleSearchOnChange,
    onClickToChoosePokemon,
  };
}
