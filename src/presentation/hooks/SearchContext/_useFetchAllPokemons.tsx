import { useEffect, useReducer, useRef, useState } from "react";
import { useInject } from "~/data/di";
import { IListPokemonItem } from "~/domain/model";
import { getListPokemonUseCase } from "~/domain/usecases";
import reducer, {
  initialState,
} from "~/shared/contexts/SearchProvider/reducer";

const useFetchAllPokemons = () => {
  const [offset] = useState(0);
  const [limit] = useState(9999);
  const [state, dispatch] = useReducer(reducer, initialState);
  const initial = useRef(true);
  const { injectPokemonRepositoryImpl } = useInject();

  const fetchAllPokemonsUseCase = async () => {
    dispatch({
      type: "LOADING",
    });

    const { results } = await getListPokemonUseCase(
      injectPokemonRepositoryImpl
    ).invoke(offset, limit);

    dispatch({ type: "GET_ALL_POKEMONS", payload: results });
  };

  useEffect(() => {
    if (initial.current) {
      fetchAllPokemonsUseCase();
    }
    initial.current = false;
  }, []);

  const realTimeSearch = (search: string) => {
    dispatch({
      type: "LOADING",
    });

    const res = state.allPokemons.filter((pokemon: IListPokemonItem) => {
      const data = pokemon.name.includes(search) || pokemon.id.includes(search);
      return data;
    });

    dispatch({ type: "GET_SEARCH", payload: res });
  };

  return {
    allPokemons: state.allPokemons,
    state,
    dispatch,
    realTimeSearch,
  };
};

export default useFetchAllPokemons;
