import { useState } from "react";
import { useRouter } from "next/navigation";
import { useInject } from "~/data/di";
import { IDetailPokemonModel } from "~/domain/model";
import { getDetailPokemonUseCase } from "~/domain/usecases";
import { handleEvolvePokemonUseCase } from "~/domain/usecases/pokemon/evolve-pokemon.usecase";
import localStorageService from "~/shared/utils/local-storage.adapter";

const useEvolvePokemon = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const localdb = localStorageService<IDetailPokemonModel>("pokemon", "secret");
  const dataPokemon = localdb.getAll<IDetailPokemonModel>();
  const { injectPokemonRepositoryImpl } = useInject();
  const router = useRouter();
  const isReadyToEvolve =
    dataPokemon.weight >= dataPokemon.maxWeight &&
    dataPokemon.name !== dataPokemon.nextEvolution;

  const fetchDetailPokemonUseCase = async (param: string | number) => {
    const data = await getDetailPokemonUseCase(
      injectPokemonRepositoryImpl
    ).invoke(param);
    return data;
  };

  const fetchEvolvePokemonUseCase = (newPokemon: IDetailPokemonModel) => {
    const data = handleEvolvePokemonUseCase(injectPokemonRepositoryImpl).invoke(
      newPokemon
    );
    return data;
  };

  const onHandlePokemonEvolveTo = async () => {
    try {
      setIsLoading(true);
      const result = await fetchDetailPokemonUseCase(dataPokemon.nextEvolution);
      const newData = fetchEvolvePokemonUseCase(result);
      setIsLoading(false);
      router.push(`/pokemon/${newData.name}`);
    } catch (err) {
      setIsLoading(false);
    }
  };

  return {
    isLoading,
    isReadyToEvolve,
    onHandlePokemonEvolveTo,
  };
};

export default useEvolvePokemon;
