import useListBerries from "./_useListBerries";
import useSelectedBerry from "./_useSelectedBerry";
import useFeedPokemon from "./_useFeedPokemon";
import useEvolvePokemon from "./_useEvolvePokemon";
import useDeletePokemon from "./_useDeletePokemon";
import useFetchDetail from "./_useFetchDetail";

export default function useDetailViewModel() {
  const { dataPokemon } = useFetchDetail();
  const { dataBerries, isLoadBerries } = useListBerries();
  const { onDeletePokemon } = useDeletePokemon();
  const { onHandleFeedPokemon } = useFeedPokemon();
  const {
    data: dataBerry,
    isLoading: isLoadBerry,
    selectBerry,
    handleSelectBerry,
  } = useSelectedBerry();
  const {
    isLoading: isLoadEvolution,
    isReadyToEvolve,
    onHandlePokemonEvolveTo,
  } = useEvolvePokemon();

  return {
    dataPokemon,
    dataBerries,
    isLoadBerries,
    dataBerry,
    isLoadBerry,
    isLoadEvolution,
    isReadyToEvolve,
    onDeletePokemon,
    handleSelectBerry,
    selectBerry,
    onHandleFeedPokemon,
    onHandlePokemonEvolveTo,
  };
}
