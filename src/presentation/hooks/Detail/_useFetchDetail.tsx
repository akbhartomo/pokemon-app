import { IDetailPokemonModel } from "~/domain/model";
import localStorageService from "~/shared/utils/local-storage.adapter";

const useFetchDetail = () => {
  const localdb = localStorageService<IDetailPokemonModel>("pokemon", "secret");
  const dataPokemon = localdb.getAll<IDetailPokemonModel>();

  return { dataPokemon };
};

export default useFetchDetail;
