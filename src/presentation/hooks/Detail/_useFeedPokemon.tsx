import {
  queryOptions,
  useMutation,
  useQueryClient,
} from "@tanstack/react-query";
import { useCallback } from "react";
import { useInject } from "~/data/di";
import { IDetailPokemonModel } from "~/domain/model";
import { handleFeedPokemonUseCase } from "~/domain/usecases/pokemon/feed-pokemon.usecase";

const useFeedPokemon = () => {
  const queryClient = useQueryClient();
  const { injectPokemonRepositoryImpl } = useInject();

  const queryDataPokemon = queryOptions({
    queryKey: ["detailPokemonState"],
  });

  const handleFeedPokemon = () => {
    const data = handleFeedPokemonUseCase(injectPokemonRepositoryImpl).invoke();
    return data;
  };

  const { mutate } = useMutation({
    mutationFn: async () => handleFeedPokemon,
    onMutate: async (newData: IDetailPokemonModel) => {
      await queryClient.cancelQueries(queryDataPokemon);

      const prevData = queryClient.getQueryData<IDetailPokemonModel>(
        queryDataPokemon.queryKey
      );

      queryClient.setQueryData(queryDataPokemon.queryKey, newData);

      return { prevData };
    },
    onError: (err, variables, context) => {
      queryClient.setQueryData(queryDataPokemon.queryKey, context?.prevData);
    },
    // Always refetch after error or success:
    onSettled: () => {
      queryClient.invalidateQueries({ queryKey: ["detailPokemonState"] });
    },
  });

  const onHandleFeedPokemon = useCallback(() => {
    mutate(handleFeedPokemon());
  }, [mutate, handleFeedPokemon]);

  return {
    onHandleFeedPokemon,
  };
};

export default useFeedPokemon;
