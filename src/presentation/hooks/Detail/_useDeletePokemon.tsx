import { useRouter } from "next/navigation";
import { IDetailBerryModel, IDetailPokemonModel } from "~/domain/model";
import localStorageService from "~/shared/utils/local-storage.adapter";

const useDeletePokemon = () => {
  const localdb = localStorageService<IDetailPokemonModel>("pokemon", "secret");
  const router = useRouter();

  const onDeletePokemon = () => {
    localdb.remove<IDetailPokemonModel>("pokemon");
    localdb.remove<IDetailBerryModel>("berry");
    router.push("/");
  };

  return {
    onDeletePokemon,
  };
};

export default useDeletePokemon;
