import { useState } from "react";
import { useInject } from "~/data/di";
import { IDetailBerryDescription } from "~/domain/model";
import { chooseBerryUseCase } from "~/domain/usecases/berries/choose-berry.usecase";
import {
  HARD,
  SOFT,
  SUPER_HARD,
  VERY_HARD,
  VERY_SOFT,
} from "~/shared/constants/firmness.constant";

const useSelectedBerry = () => {
  const [selectBerry, setSelectedBerry] = useState<number | undefined>(0);
  const [data, setData] = useState<IDetailBerryDescription | null>(null);
  const [isLoading, setisLoading] = useState<boolean>(false);
  const { injectBerriesRepositoryImpl } = useInject();

  const mapFirmnessPoint = (berry: string) => {
    let point = 0;
    let firmness = "";

    switch (berry) {
      case VERY_SOFT:
        point = 2;
        firmness = "Very Soft";
        break;
      case SOFT:
        point = 3;
        firmness = "Soft";
        break;
      case HARD:
        point = 5;
        firmness = "Hard";
        break;
      case VERY_HARD:
        point = 8;
        firmness = "Very Hard";
        break;
      case SUPER_HARD:
        point = 10;
        firmness = "Super Hard";
        break;
      default:
        break;
    }

    return { point, firmness };
  };

  const handleSelectBerry = async (id: number) => {
    setSelectedBerry(id);
    try {
      setisLoading(true);
      const data = await chooseBerryUseCase(injectBerriesRepositoryImpl).invoke(
        id
      );
      const dataFirmness = mapFirmnessPoint(data.firmness);
      setData(dataFirmness);
      setisLoading(false);
    } catch (err) {
      setisLoading(false);
    } finally {
      setisLoading(false);
    }
  };

  return { selectBerry, handleSelectBerry, data, isLoading };
};

export default useSelectedBerry;
