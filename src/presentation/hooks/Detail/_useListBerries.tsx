import { useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { useInject } from "~/data/di";
import { getListBerriesUseCase } from "~/domain/usecases";

const useListBerries = () => {
  const [limit] = useState(99);
  const { injectBerriesRepositoryImpl } = useInject();

  const fetchListBerries = async (limit: number) => {
    const { results } = await getListBerriesUseCase(
      injectBerriesRepositoryImpl
    ).invoke(limit);

    return results;
  };

  const {
    data: dataBerries,
    isLoading: isLoadBerries,
    isFetching: isFetchBerries,
  } = useQuery({
    queryKey: ["berriesPokemonState"],
    queryFn: () => fetchListBerries(limit),
  });

  return {
    dataBerries,
    isLoadBerries,
    isFetchBerries,
  };
};

export default useListBerries;
