import useFetchListPokemon from "./_useFetchListPokemon";
import useSelectedPokemon from "./_useSelectedPokemon";

export default function useHomeViewModel() {
  const {
    data: dataPokemon,
    selectedItem,
    handleSelectedPokemon,
    handleChoosePokemon,
    isLoading,
  } = useSelectedPokemon();

  const {
    dataListPokemons,
    dataIsLoading,
    hasNextPage,
    isFetchingNextPage,
    isFetchingPreviousPage,
    refInView,
    statusListPokemons,
  } = useFetchListPokemon();

  return {
    dataPokemon,
    dataListPokemons,
    dataIsLoading,
    statusListPokemons,
    refInView,
    isFetchingNextPage,
    isFetchingPreviousPage,
    isLoading,
    hasNextPage,
    selectedItem,
    handleSelectedPokemon,
    handleChoosePokemon,
  };
}
