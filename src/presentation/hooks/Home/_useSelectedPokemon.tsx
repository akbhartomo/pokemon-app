import { useState } from "react";
import { useRouter } from "next/navigation";
import { useInject } from "~/data/di";
import { getDetailPokemonUseCase } from "~/domain/usecases";
import { IDetailPokemonModel } from "~/domain/model";

const useSelectedPokemon = () => {
  const [selectedItem, setSelectedItem] = useState<string>("");
  const [data, setData] = useState<IDetailPokemonModel>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { injectPokemonRepositoryImpl } = useInject();
  const router = useRouter();

  const handleSelectedPokemon = (name?: string) => {
    setSelectedItem(`${name}`);
  };

  const fetchDetailPokemonUseCase = async (param: string | number) => {
    const data = await getDetailPokemonUseCase(
      injectPokemonRepositoryImpl
    ).invoke(param);

    return data;
  };

  const handleChoosePokemon = async () => {
    try {
      setIsLoading(true);
      const result = await fetchDetailPokemonUseCase(selectedItem);
      setData(result);
      setIsLoading(false);
      router.push(`/pokemon/${result.name}`);
    } catch (err) {
      setIsLoading(false);
    } finally {
      setIsLoading(false);
    }
  };

  return {
    selectedItem,
    handleSelectedPokemon,
    handleChoosePokemon,
    data,
    isLoading,
  };
};

export default useSelectedPokemon;
