import { useEffect, useState } from "react";
import { useInView } from "react-intersection-observer";
import { useInfiniteQuery } from "@tanstack/react-query";
import { useInject } from "~/data/di";
import { getListPokemonUseCase } from "~/domain/usecases";

const useFetchListPokemon = () => {
  const [offset] = useState(0);
  const [limit] = useState(20);
  const { ref, inView } = useInView();
  const { injectPokemonRepositoryImpl } = useInject();

  const fetchListPokemonUseCase = async (offset: number, limit: number) => {
    const { results } = await getListPokemonUseCase(
      injectPokemonRepositoryImpl
    ).invoke(offset, limit);

    return results;
  };

  const {
    data,
    status,
    isLoading,
    isFetching,
    isFetchingNextPage,
    isFetchingPreviousPage,
    fetchNextPage,
    hasNextPage,
  } = useInfiniteQuery({
    queryKey: ["listPokemonState"],
    queryFn: async ({ pageParam }) => {
      const res = await fetchListPokemonUseCase(pageParam, limit);
      return res;
    },
    initialPageParam: offset,
    getNextPageParam: (lastPage, allPages) => {
      const lp = offset < 20 ? +lastPage.length * +allPages.length : null;
      return lp ?? undefined;
    },
  });

  useEffect(() => {
    if (inView) {
      fetchNextPage();
    }
  }, [fetchNextPage, inView]);

  return {
    dataListPokemons: data,
    dataIsLoading: isLoading,
    statusListPokemons: status,
    refInView: ref,
    isFetching,
    isFetchingNextPage,
    isFetchingPreviousPage,
    hasNextPage,
  };
};

export default useFetchListPokemon;
