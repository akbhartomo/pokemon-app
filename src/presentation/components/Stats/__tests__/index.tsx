import React from "react";
import { render, fireEvent } from "~/test";
import Stats from "..";
import { IDetailPokemonModel } from "~/domain/model";

// Mocking the next/legacy/image module
jest.mock("next/legacy/image", () => {
  return ({ src, alt }: any) => <img src={src} alt={alt} />;
});

const mockPokemonData: IDetailPokemonModel = {
  name: "Pikachu",
  image: "pikachu.jpg",
  stats: [
    { stat: { name: "hp" }, base_stat: 50 },
    { stat: { name: "attack" }, base_stat: 70 },
    { stat: { name: "defense" }, base_stat: 40 },
  ],
  weight: 60,
  id: 0,
  nextEvolution: "",
  maxWeight: 0,
  prevMeal: "",
};

describe("Stats component", () => {
  test("renders Pokemon name and stats correctly", () => {
    const { getByText } = render(
      <Stats
        data={mockPokemonData}
        isLoading={false}
        isReadyToEvolve={false}
        onEvolvePokemon={() => {}}
      />
    );

    // Check if Pokemon name is rendered
    const pokemonName = getByText("Pikachu");
    expect(pokemonName).toBeInTheDocument();

    // Check if Pokemon stats are rendered
    expect(getByText("hp")).toBeInTheDocument();
    expect(getByText("attack")).toBeInTheDocument();
    expect(getByText("defense")).toBeInTheDocument();
    expect(getByText("60")).toBeInTheDocument(); // Weight
  });

  test("renders evolution button and triggers onEvolvePokemon when clicked", () => {
    const onEvolvePokemonMock = jest.fn();
    const { getByText } = render(
      <Stats
        data={mockPokemonData}
        isLoading={false}
        isReadyToEvolve={true}
        onEvolvePokemon={onEvolvePokemonMock}
      />
    );

    // Check if evolution button is rendered
    const evolutionButton = getByText("Evolution");
    expect(evolutionButton).toBeInTheDocument();

    // Click on the evolution button
    fireEvent.click(evolutionButton);

    // Ensure onEvolvePokemon is called
    expect(onEvolvePokemonMock).toHaveBeenCalledTimes(1);
  });

  test("shows loading spinner when isLoading is true", () => {
    const { queryByTestId } = render(
      <Stats
        data={mockPokemonData}
        isLoading={true}
        isReadyToEvolve={true}
        onEvolvePokemon={() => {}}
        data-testid="spinner"
      />
    );

    // Check if loading spinner is rendered
    const loadingSpinner = queryByTestId("spinner");
    expect(loadingSpinner).toBeDefined();
  });
});
