"use client";

import { Button, Flex, Text } from "@chakra-ui/react";
import Image from "next/legacy/image";
import RenderIf from "../RenderIf";
import type { IDetailPokemonModel } from "~/domain/model";
import { BeatLoader } from "react-spinners";

export interface IStatusPokemonProps {
  data: IDetailPokemonModel;
  isLoading: boolean;
  isReadyToEvolve: boolean;
  onEvolvePokemon: () => void;
  children?: React.ReactNode;
}

const Stats: React.FC<IStatusPokemonProps> = ({
  data,
  isLoading,
  isReadyToEvolve,
  onEvolvePokemon,
}) => {
  return (
    <>
      <Flex>
        <Text fontSize="2xl" fontWeight="800" textTransform="capitalize">
          {data.name}
        </Text>
      </Flex>
      <Flex
        justifyContent="center"
        alignItems="center"
        height={180}
        objectFit="fill"
      >
        <RenderIf isTrue={!!data.image}>
          <Image
            src={`${data.image}`}
            alt={"pokemon-name-" + data.name}
            width={180}
            height={180}
            placeholder="blur"
            blurDataURL={`${data.image}`}
          />
        </RenderIf>
      </Flex>
      <RenderIf isTrue={!!isReadyToEvolve}>
        <Button
          bgColor="warning.main"
          color="general.dark"
          colorScheme="warning.main"
          border="4px solid"
          borderColor="warning.mid"
          onClick={onEvolvePokemon}
          isLoading={isLoading || !data}
          spinner={<BeatLoader size={8} color="gray" />}
          _active={{
            bg: "yellow.200",
            borderColor: "warning.main",
          }}
        >
          Evolution
        </Button>
      </RenderIf>
      <Flex maxW="328px" justifyContent="center">
        <Flex flexDirection="column">
          {data?.stats?.map((stat, idx) => (
            <Flex minW={100} key={idx}>
              <Text minW="100px" fontSize="md" textTransform="uppercase">
                {stat.stat.name}
              </Text>
              <Text
                minW="48px"
                textAlign="right"
                fontSize="md"
                fontWeight="800"
              >
                {stat.base_stat}
              </Text>
            </Flex>
          ))}
          <Flex minW={100}>
            <Text minW="100px" fontSize="md" textTransform="uppercase">
              Weight
            </Text>
            <Text minW="48px" textAlign="right" fontSize="md" fontWeight="700">
              {data.weight}
            </Text>
          </Flex>
        </Flex>
      </Flex>
    </>
  );
};

export default Stats;
