import { Flex } from "@chakra-ui/react";
import styles from "./styles.module.css";

const Loader = () => {
  return (
    <Flex flexDirection="row" transform="scale(0.35)">
      <div id={styles.pikachu}></div>
      <div id={styles.ash}></div>
    </Flex>
  );
};

export default Loader;
