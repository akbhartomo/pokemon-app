import React from "react";
import { render } from "~/test";
import Loader from "..";
import styles from "../styles.module.css";

describe("Loader component", () => {
  test("renders Loader component correctly", () => {
    const { container } = render(<Loader />);

    // Check if Loader component is rendered
    expect(container.firstChild).toBeInTheDocument();

    // Check if Pikachu and Ash elements are rendered with correct styles
    const pikachu = container.querySelector(`#${styles.pikachu}`);
    expect(pikachu).toBeInTheDocument();

    const ash = container.querySelector(`#${styles.ash}`);
    expect(ash).toBeInTheDocument();
  });
});
