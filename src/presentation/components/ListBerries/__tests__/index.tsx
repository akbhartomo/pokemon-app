import React from "react";
import { render, fireEvent } from "~/test";
import ListBerries from "..";

// Mocking the next/legacy/image module
jest.mock("next/legacy/image", () => {
  return ({ src, alt }: any) => <img src={src} alt={alt} />;
});

const mockData = [
  { id: 1, name: "Cherry", image: "cherry.jpg" },
  { id: 2, name: "Blueberry", image: "blueberry.jpg" },
];

describe("ListBerries component", () => {
  test("renders berries correctly when data is available", () => {
    const onSelectMock = jest.fn();
    const { getByAltText, getByText } = render(
      <ListBerries data={mockData} isLoading={false} onSelect={onSelectMock} />
    );

    // Check if berries are rendered
    mockData.forEach((berry) => {
      const berryImage = getByAltText(`${berry.name}-berry`);
      expect(berryImage).toBeInTheDocument();

      const berryName = getByText(berry.name);
      expect(berryName).toBeInTheDocument();
    });
  });

  test("renders loading spinner when isLoading is true", () => {
    const { queryByTestId } = render(
      <ListBerries isLoading={true} onSelect={() => {}} data-testid="spinner" />
    );

    // Check if loading spinner is rendered
    const spinner = queryByTestId("spinner");
    expect(spinner).toBeDefined();
  });

  test("calls onSelect with correct id when a berry is clicked", () => {
    const onSelectMock = jest.fn();
    const { getByAltText } = render(
      <ListBerries data={mockData} isLoading={false} onSelect={onSelectMock} />
    );

    // Click on the first berry
    fireEvent.click(getByAltText("Cherry-berry"));

    // Ensure onSelect is called with the correct id
    expect(onSelectMock).toHaveBeenCalledTimes(1);
    expect(onSelectMock).toHaveBeenCalledWith(1);
  });
});
