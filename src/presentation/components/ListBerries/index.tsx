"use client";

import Image from "next/legacy/image";
import { Box, Flex, Spinner, Text } from "@chakra-ui/react";
import type { IListBerryItem } from "~/domain/model";
import RenderIf from "../RenderIf";

interface IListBerries {
  data?: IListBerryItem[];
  isLoading: boolean;
  selected?: number;
  onSelect: (id: number) => void;
  children?: React.ReactNode;
}

const ListBerries: React.FC<IListBerries> = ({
  data,
  isLoading,
  selected,
  onSelect,
}) => {
  return (
    <Flex
      maxWidth="calc(100dvw - 36px)"
      border="1px solid"
      borderColor="warning.mid"
      borderRadius={10}
      overflow="hidden"
    >
      <Flex as="main" whiteSpace="nowrap" padding={1} overflowX="auto">
        <RenderIf isTrue={isLoading}>
          <Flex w="calc(100dvw - 36px)" p={4} justifyContent="center">
            <Spinner color="primary.main" />
          </Flex>
        </RenderIf>
        <RenderIf isTrue={!!data}>
          {data?.map((berry, idx) => {
            return (
              <Flex
                alignItems="center"
                flexDirection="column"
                key={idx}
                mt={2}
                mb={1}
                mx={1}
                minWidth="64px"
                backgroundColor={
                  selected === berry.id ? "warning.main" : undefined
                }
                borderRadius={selected === berry.id ? 8 : undefined}
                boxShadow={
                  selected === berry.id
                    ? "0 0 0 2px rgba(155, 17, 66, 1)"
                    : undefined
                }
                transition={
                  selected === berry.id ? "all 0.12s ease-in-out" : undefined
                }
                onClick={() => onSelect(berry.id)}
                cursor="pointer"
              >
                <Box width="40px" height="40px">
                  <Image
                    src={berry.image}
                    alt={`${berry.name}-berry`}
                    width={40}
                    height={40}
                    placeholder="blur"
                    blurDataURL={berry.image}
                  />
                </Box>
                <Text fontSize="sm" mb={1}>
                  {berry.name}
                </Text>
              </Flex>
            );
          })}
        </RenderIf>
      </Flex>
    </Flex>
  );
};

export default ListBerries;
