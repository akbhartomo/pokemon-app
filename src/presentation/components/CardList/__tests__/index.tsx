import React from "react";
import { render, fireEvent } from "~/test";
import CardList from "..";

// Mocking the next/legacy/image module
jest.mock("next/legacy/image", () => {
  return ({ src, alt }: any) => <img src={src} alt={alt} />;
});

const mockData = [
  {
    id: "1",
    image: "pikachu.jpg",
    name: "Pikachu",
    url: "https://example.com",
  },
  {
    id: "2",
    image: "charmander.jpg",
    name: "Charmander",
    url: "https://example.com",
  },
];

describe("CardList component", () => {
  test("renders card list correctly", () => {
    const handleSelectedPokemonMock = jest.fn();
    const { getByText } = render(
      <CardList
        data={mockData}
        selectedItem="Pikachu"
        handleSelectedPokemon={handleSelectedPokemonMock}
      />
    );

    // Ensure all cards are rendered
    // const cards = getAllByTestId("CardListPokemon");
    // expect(cards.length).toBe(mockData.length);

    // Ensure the selected item is correctly highlighted
    // expect(getByText("Pikachu")).toHaveStyle(
    //   "box-shadow: inset 0px 0px 0px 3px #17809f"
    // );
    expect(getByText("Pikachu")).toMatchSnapshot();
  });

  test("calls handleSelectedPokemon when a card is clicked", () => {
    const handleSelectedPokemonMock = jest.fn();
    const { getByText } = render(
      <CardList
        data={mockData}
        selectedItem="Pikachu"
        handleSelectedPokemon={handleSelectedPokemonMock}
      />
    );

    // Click on Charmander card
    fireEvent.click(getByText("Charmander"));

    // Ensure handleSelectedPokemon is called with correct argument
    expect(handleSelectedPokemonMock).toHaveBeenCalledTimes(1);
    expect(handleSelectedPokemonMock).toHaveBeenCalledWith("Charmander");
  });
});
