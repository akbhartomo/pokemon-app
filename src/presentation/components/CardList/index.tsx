import { Box, Flex } from "@chakra-ui/react";
import { IListPokemonItem } from "~/domain/model";
import Card from "../Card";

interface TDataCardListProps {
  data: Array<IListPokemonItem>;
  selectedItem?: string;
  handleSelectedPokemon: (name?: string) => void;
}

const CardList: React.FC<TDataCardListProps> = ({
  data,
  selectedItem,
  handleSelectedPokemon,
}) => {
  return (
    <Box maxW={400}>
      <Flex flexWrap="wrap" justifyContent="center">
        {data.map((item: IListPokemonItem, idx: number) => (
          <Card
            card={item}
            key={idx}
            isSelected={item.name === selectedItem}
            onSelect={() => handleSelectedPokemon(item.name)}
          />
        ))}
      </Flex>
    </Box>
  );
};

export default CardList;
