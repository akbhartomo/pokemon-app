import { FunctionComponent } from "react";
import { Box } from "@chakra-ui/react";
import Header from "../Header";
import styles from "./styles.module.css";

type TLayoutProps = {
  children?: React.ReactNode;
  title?: string;
  search?: React.ReactNode;
};

const Layout: FunctionComponent<TLayoutProps> = ({
  title,
  search,
  children,
}) => {
  return (
    <Box className={styles.poke_app}>
      <Box as="header">
        <Header title={title} search={search} />
      </Box>
      <Box
        as="main"
        backgroundColor="background.primary"
        backgroundImage="url(/bg-frame.webp)"
        backgroundRepeat="no-repeat"
        backgroundSize="cover"
      >
        <Box pb="16px" pt="8px">
          {children}
        </Box>
      </Box>
    </Box>
  );
};

export default Layout;
