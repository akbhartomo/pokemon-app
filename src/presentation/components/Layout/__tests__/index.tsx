import React from "react";
import { render } from "~/test";
import Layout from "..";

describe("Layout component", () => {
  test("renders children and passes title and search props to Header component", () => {
    const title = "Pokédex";
    const search = <input placeholder="Search Pokémon" />;
    const { getByText, getByPlaceholderText } = render(
      <Layout title={title} search={search}>
        <div>Child Component</div>
      </Layout>
    );

    // Check if child component is rendered
    const childComponent = getByText("Child Component");
    expect(childComponent).toBeInTheDocument();

    // Check if title is passed to Header component
    const titleElement = getByText("Pokédex");
    expect(titleElement).toBeInTheDocument();

    // Check if search is passed to Header component
    const searchInput = getByPlaceholderText("Search Pokémon");
    expect(searchInput).toBeInTheDocument();
  });
});
