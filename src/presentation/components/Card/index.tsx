import Image from "next/legacy/image";
import {
  Card as CardChakra,
  CardBody,
  Text,
  CardProps,
  Box,
} from "@chakra-ui/react";
import { IListPokemonItem } from "~/domain/model";

interface ICardPokemon {
  card: IListPokemonItem;
  isSelected: boolean;
  onSelect: (card: IListPokemonItem) => void;
}

type TChakraCardProps = CardProps & ICardPokemon;

const Card: React.FC<TChakraCardProps> = ({
  card,
  isSelected,
  onSelect,
  ...restProps
}) => {
  return (
    <CardChakra
      display="flex"
      alignItems="center"
      background="rgba(255, 255, 255, 0.75);"
      w="100%"
      minW="84px"
      maxW="10dvh"
      margin="2px"
      padding={3}
      boxShadow={isSelected ? "inset 0px 0px 0px 3px #17809f" : undefined}
      cursor="pointer"
      onClick={() => onSelect(card)}
      {...restProps}
    >
      <CardBody p={0}>
        <Box w="60px" h="60px">
          <Image
            src={card.image}
            alt={"pokemon-" + card.name}
            width={60}
            height={60}
            placeholder="blur"
            blurDataURL={card.image}
            quality={60}
          />
        </Box>
      </CardBody>
      <CardBody pt={3} pb={0} px={0} textAlign="center">
        <Text fontSize="xx-small" fontWeight="600" color="yellow.700">
          #{card.id}
        </Text>
        <Text fontSize="x-small" fontWeight="600" color="primary.dark">
          {card.name}
        </Text>
      </CardBody>
    </CardChakra>
  );
};

export default Card;
