import React from "react";
import { render, fireEvent } from "~/test";
import Card from "..";

// Mocking the next/legacy/image module
jest.mock("next/legacy/image", () => {
  return ({ src, alt }: any) => <img src={src} alt={alt} />;
});

const mockCard = {
  id: "1",
  image: "pikachu.png",
  name: "Pikachu",
  url: "https://example.com",
};

describe("Card component", () => {
  test("renders card correctly", () => {
    const onSelectMock = jest.fn();
    const { getByAltText, getByText } = render(
      <Card card={mockCard} isSelected={false} onSelect={onSelectMock} />
    );

    const image = getByAltText("pokemon-Pikachu");
    expect(image).toBeInTheDocument();
    expect(image.getAttribute("src")).toBe("pikachu.png");

    const idText = getByText("#1");
    expect(idText).toBeInTheDocument();

    const nameText = getByText("Pikachu");
    expect(nameText).toBeInTheDocument();
  });

  test("calls onSelect when clicked", () => {
    const onSelectMock = jest.fn();
    const { getByTestId } = render(
      <Card
        card={mockCard}
        isSelected={false}
        onSelect={onSelectMock}
        data-testid="test-card"
      />
    );

    const card = getByTestId("test-card");
    fireEvent.click(card);

    expect(onSelectMock).toHaveBeenCalledTimes(1);
    expect(onSelectMock).toHaveBeenCalledWith(mockCard);
  });
});
