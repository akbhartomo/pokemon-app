import React from "react";
import { render } from "~/test";
import Berry from "..";

const mockData = {
  firmness: "Hard",
  point: 50,
};

describe("Berry component", () => {
  test("renders berry information correctly when data is available", () => {
    const { getByText, queryByTestId } = render(
      <Berry data={mockData} isLoading={false} data-testid="skeleton" />
    );

    // Check if skeleton is not rendered
    const skeleton = queryByTestId("skeleton");
    expect(skeleton).toBeNull();

    // Check if firmness information is rendered correctly
    const firmnessText = getByText("Firmness:");
    expect(firmnessText).toBeInTheDocument();
    const firmnessValue = getByText("Hard");
    expect(firmnessValue).toBeInTheDocument();

    // Check if point information is rendered correctly
    const pointText = getByText("Poin:");
    expect(pointText).toBeInTheDocument();
    const pointValue = getByText("50");
    expect(pointValue).toBeInTheDocument();
  });

  test("renders loading skeleton when isLoading is true", () => {
    const { queryByTestId } = render(
      <Berry data={null} isLoading={true} data-testid="skeleton" />
    );

    // Check if skeleton is rendered
    const skeleton = queryByTestId("skeleton");
    expect(skeleton).toBeDefined();
  });

  test("does not render berry information when data is null", () => {
    const { queryByText } = render(<Berry data={null} isLoading={false} />);

    // Check if berry information is not rendered
    const firmnessText = queryByText("Firmness:");
    expect(firmnessText).toBeNull();
    const pointText = queryByText("Poin:");
    expect(pointText).toBeNull();
  });
});
