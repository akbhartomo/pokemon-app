"use client";

import { Flex, Skeleton, Text } from "@chakra-ui/react";
import { IDetailBerryDescription } from "~/domain/model";
import RenderIf from "../RenderIf";

interface IBerry {
  data: IDetailBerryDescription | null;
  isLoading: boolean;
}

const Berry: React.FC<IBerry> = ({ data, isLoading }) => {
  return (
    <RenderIf isTrue={!!data}>
      <Skeleton
        startColor="general.light"
        endColor="warning.mid"
        height="24px"
        isLoaded={!data || !isLoading}
        borderRadius={8}
      >
        <Flex gap={4}>
          <Text>
            Firmness:{" "}
            <Text as="span" color="green.600" fontWeight="800">
              {data?.firmness}
            </Text>
          </Text>
          <Text>
            Poin:{" "}
            <Text as="span" color="red.700" fontWeight="800">
              {data?.point}
            </Text>
          </Text>
        </Flex>
      </Skeleton>
    </RenderIf>
  );
};

export default Berry;
