"use client";

import React, { useEffect } from "react";
import { Box, Card, Flex, Input, Stack, Text } from "@chakra-ui/react";
import { useSearchContext } from "~/shared/contexts/SearchProvider";
import RenderIf from "../RenderIf";
import type { IListPokemonItem } from "~/domain/model";
import Image from "next/legacy/image";
import useSearchViewModel from "~/presentation/hooks/SearchContext/useSearchViewModel";

const Search: React.FC = () => {
  const { searchResult, realTimeSearch }: any = useSearchContext();
  const {
    searchQuery,
    debounceSearch,
    handleSearchOnChange,
    onClickToChoosePokemon,
  } = useSearchViewModel();

  useEffect(() => {
    realTimeSearch(debounceSearch);
  }, [debounceSearch]);

  return (
    <Box position="relative">
      <Input
        placeholder="Search pokemon name or id..."
        bgColor="white"
        color="general.main"
        w="100%"
        value={searchQuery}
        onChange={handleSearchOnChange}
      />
      <RenderIf isTrue={!!debounceSearch && searchResult.length > 0}>
        <Card
          overflow="hidden"
          borderRadius="6px"
          position="absolute"
          top="42px"
          width="100%"
        >
          <Box
            as="main"
            py="12px"
            width="100%"
            maxHeight="300px"
            bgColor="white"
            overflowY="auto"
          >
            <Stack as="section" spacing={3}>
              {searchResult.map((pokemon: IListPokemonItem, idx: number) => (
                <Flex
                  alignItems="center"
                  key={idx}
                  px="16px"
                  onClick={() => onClickToChoosePokemon(pokemon.name)}
                  cursor="pointer"
                  _active={{
                    background: "rgba(0, 0, 0, 0.1)",
                  }}
                >
                  <Box w="50px" h="40px" display="flex" alignItems="center">
                    <Image
                      src={pokemon.image}
                      alt={"pokemon" + pokemon.name}
                      width={40}
                      height={40}
                      placeholder="blur"
                      blurDataURL={pokemon.image}
                      quality={60}
                    />
                  </Box>
                  <Text color="primary.dark" fontSize="md" fontWeight="500">
                    {pokemon.name}
                  </Text>
                </Flex>
              ))}
            </Stack>
          </Box>
        </Card>
      </RenderIf>
    </Box>
  );
};

export default Search;
