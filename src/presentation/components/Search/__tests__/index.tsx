import { render } from "~/test";
import Search from "..";
import { useSearchContext } from "~/shared/contexts/SearchProvider";

jest.mock("next/navigation", () => ({
  useRouter: () => ({
    pathname: "pikachu",
  }),
}));

jest.mock("~/shared/contexts/SearchProvider", () => ({
  useSearchContext: jest.fn(),
}));

jest.mock("~/presentation/hooks/SearchContext/useSearchViewModel", () => ({
  __esModule: true,
  default: jest.fn(() => ({
    searchQuery: "",
    debounceSearch: "",
    handleSearchOnChange: jest.fn(),
    onClickToChoosePokemon: jest.fn(),
    realTimeSearch: jest.fn(),
    searchResult: [{ name: "Pikachu", image: "pikachu.jpg" }],
  })),
}));

describe("Search component", () => {
  beforeEach(() => {
    (useSearchContext as jest.Mock).mockReturnValue({
      searchResult: [],
      realTimeSearch: jest.fn(),
    });
  });

  test("renders input field correctly", () => {
    const { getByPlaceholderText } = render(<Search />);
    const inputField = getByPlaceholderText("Search pokemon name or id...");
    expect(inputField).toBeInTheDocument();
  });

  test("Home test to match snapshot", () => {
    const { asFragment } = render(<Search />);
    expect(asFragment()).toMatchSnapshot();
  });
});
