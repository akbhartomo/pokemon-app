import React from "react";
import { render } from "~/test";
import QueryProvider from "..";
import { QueryClient } from "@tanstack/react-query";

jest.mock("react", () => ({
  ...jest.requireActual("react"),
  useState: jest.fn(),
}));

describe("QueryProvider component", () => {
  beforeEach(() => {
    // Reset the mock implementation before each test
    (React.useState as jest.Mock).mockReset();
  });

  test("renders children wrapped in QueryClientProvider with a QueryClient instance", () => {
    const queryClientMock = new QueryClient();
    (React.useState as jest.Mock).mockReturnValueOnce([
      queryClientMock,
      jest.fn(),
    ]);

    const { getByText } = render(
      <QueryProvider>
        <div>Child Component</div>
      </QueryProvider>
    );

    // Check if QueryClientProvider is rendered
    const queryClientProvider = getByText("Child Component").parentElement;

    // Check if QueryClientProvider is wrapped around the children
    expect(queryClientProvider?.tagName).toBe("DIV");
  });
});
