import { memo, Fragment } from "react";
import { Flex, Stack, Text } from "@chakra-ui/react";
import RenderIf from "../RenderIf";
import styles from "./styles.module.css";

type THeaderProps = {
  children?: React.ReactNode;
  title?: string;
  search?: React.ReactNode;
};

const Header: React.FC<THeaderProps> = ({ title, search }) => {
  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      px="4"
      bgColor="background.primary"
      borderBottom="1px solid"
      borderColor="warning.mid"
      className={styles.header}
    >
      <Flex maxWidth={400} width="100%">
        <Stack spacing={3} w="100%">
          <Flex direction="column" alignItems="center" w="100%" height="30px">
            <img src="/pokemon-logo-s.svg" alt="logo" width="80" height="30" />
          </Flex>
          <RenderIf isTrue={!!search}>
            <Fragment>{search}</Fragment>
          </RenderIf>
          <RenderIf isTrue={!!title}>
            <Text
              fontSize="2xl"
              fontWeight="700"
              textAlign="center"
              textTransform="capitalize"
            >
              {title}
            </Text>
          </RenderIf>
        </Stack>
      </Flex>
    </Flex>
  );
};

export default memo(Header);
