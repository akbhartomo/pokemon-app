import React from "react";
import { render } from "~/test";
import Header from "..";

describe("Header component", () => {
  test("renders header with title and search correctly", () => {
    const title = "Pokédex";
    const search = <input placeholder="Search Pokémon" />;
    const { getByText, getByPlaceholderText } = render(
      <Header title={title} search={search} />
    );

    // Check if the title is rendered
    const titleElement = getByText("Pokédex");
    expect(titleElement).toBeInTheDocument();

    // Check if the search input is rendered
    const searchInput = getByPlaceholderText("Search Pokémon");
    expect(searchInput).toBeInTheDocument();
  });

  test("renders header with only title correctly", () => {
    const title = "Pokédex";
    const { getByText, queryByPlaceholderText } = render(
      <Header title={title} />
    );

    // Check if the title is rendered
    const titleElement = getByText("Pokédex");
    expect(titleElement).toBeInTheDocument();

    // Check if the search input is not rendered
    const searchInput = queryByPlaceholderText("Search Pokémon");
    expect(searchInput).toBeNull();
  });

  test("renders header with only search correctly", () => {
    const search = <input placeholder="Search Pokémon" />;
    const { queryByText, getByPlaceholderText } = render(
      <Header search={search} />
    );

    // Check if the title is not rendered
    const titleElement = queryByText("Pokédex");
    expect(titleElement).toBeNull();

    // Check if the search input is rendered
    const searchInput = getByPlaceholderText("Search Pokémon");
    expect(searchInput).toBeInTheDocument();
  });

  test("renders header without title and search correctly", () => {
    const { queryByText, queryByPlaceholderText } = render(<Header />);

    // Check if the title is not rendered
    const titleElement = queryByText("Pokédex");
    expect(titleElement).toBeNull();

    // Check if the search input is not rendered
    const searchInput = queryByPlaceholderText("Search Pokémon");
    expect(searchInput).toBeNull();
  });
});
