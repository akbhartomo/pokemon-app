import { Flex } from "@chakra-ui/react";

type TFooterProps = {
  children?: React.ReactNode;
};

const Footer: React.FC<TFooterProps> = ({ children }) => {
  return (
    <Flex
      position="fixed"
      alignItems="center"
      bottom="0"
      left="0"
      zIndex="2"
      w="100%"
      minH="64px"
      bgColor="background.primary"
      borderTop="1px solid"
      borderColor="warning.mid"
      justifyContent="center"
    >
      <Flex maxWidth={414} width="100%" justifyContent="center">
        {children}
      </Flex>
    </Flex>
  );
};

export default Footer;
