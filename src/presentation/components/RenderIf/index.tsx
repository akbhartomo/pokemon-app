"use client";

import React from "react";

interface IRenderIfProps {
  readonly children: React.ReactNode;
  readonly isTrue: boolean;
}

const RenderIf: React.FC<IRenderIfProps> = ({ children, isTrue }) =>
  isTrue ? children : null;

export default React.memo(RenderIf);
