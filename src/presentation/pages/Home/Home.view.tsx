"use client";

import { Box, Button, Flex, Spinner, Text } from "@chakra-ui/react";
import CardList from "~/presentation/components/CardList";
import Footer from "~/presentation/components/Footer";
import RenderIf from "~/presentation/components/RenderIf";
import useHomeTemplateViewModel from "~/presentation/hooks/Home/useHomeViewModel";
import styles from "./styles.module.css";
import Loader from "~/presentation/components/Loader";
import { BeatLoader } from "react-spinners";

export const HomeTemplate: React.FC = () => {
  const {
    dataPokemon,
    dataListPokemons,
    statusListPokemons,
    isFetchingNextPage,
    refInView,
    hasNextPage,
    selectedItem,
    handleSelectedPokemon,
    handleChoosePokemon,
    isLoading,
  } = useHomeTemplateViewModel();

  return (
    <>
      <Flex flexDirection="column">
        <Box className={styles.main}>
          <RenderIf isTrue={statusListPokemons === "pending"}>
            <Loader />
          </RenderIf>
          <RenderIf isTrue={statusListPokemons === "error"}>
            <Text>It seems network trouble!</Text>
          </RenderIf>
          <RenderIf isTrue={!!dataListPokemons}>
            <Box>
              {dataListPokemons?.pages.map((page, key) => (
                <CardList
                  data={page}
                  key={key}
                  selectedItem={selectedItem}
                  handleSelectedPokemon={handleSelectedPokemon}
                />
              ))}
              <Flex ref={refInView} justifyContent="center" minH="40px">
                <RenderIf isTrue={!hasNextPage || isFetchingNextPage}>
                  <RenderIf isTrue={isFetchingNextPage}>
                    <Box mt={4}>
                      <Spinner size="md" color="primary.dark" />
                    </Box>
                  </RenderIf>
                  <RenderIf isTrue={!hasNextPage}>
                    <Text>Nothing more loaded pokemon!</Text>
                  </RenderIf>
                </RenderIf>
              </Flex>
            </Box>
          </RenderIf>
        </Box>
      </Flex>
      <Footer>
        <Button
          mx="1rem"
          size="lg"
          width="100%"
          isDisabled={!selectedItem}
          isLoading={isLoading && !dataPokemon}
          spinner={<BeatLoader size={8} color="white" />}
          onClick={handleChoosePokemon}
        >
          I Choose You
        </Button>
      </Footer>
    </>
  );
};

export default HomeTemplate;
