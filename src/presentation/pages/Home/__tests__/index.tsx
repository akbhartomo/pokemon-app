import { render } from "~/test";
import HomeTemplate from "..";

jest.mock("next/navigation", () => ({
  useRouter: () => ({
    pathname: "pikachu",
  }),
}));

it("renders loading spinner when status is pending", async () => {
  const { queryByTestId } = render(<HomeTemplate data-testid="loader" />);
  expect(queryByTestId("loader")).toBeDefined();
});

it('renders "I Choose You" button correctly', async () => {
  const { getByText } = render(<HomeTemplate />);
  expect(getByText("I Choose You")).toBeInTheDocument();
});

it("Home test to match snapshot", () => {
  const { asFragment } = render(<HomeTemplate />);
  expect(asFragment()).toMatchSnapshot();
});
