"use client";

import React from "react";
import { CloseIcon } from "@chakra-ui/icons";
import { Button, Flex, Stack } from "@chakra-ui/react";
import Berry from "~/presentation/components/Berry";
import Footer from "~/presentation/components/Footer";
import ListBerries from "~/presentation/components/ListBerries";
import Stats from "~/presentation/components/Stats";
import useDetailViewModel from "~/presentation/hooks/Detail/useDetailViewModel";

const DetailTemplate: React.FC = () => {
  const {
    dataPokemon,
    dataBerries,
    handleSelectBerry,
    selectBerry,
    dataBerry,
    isLoadBerry,
    isLoadBerries,
    isLoadEvolution,
    isReadyToEvolve,
    onDeletePokemon,
    onHandleFeedPokemon,
    onHandlePokemonEvolveTo,
  } = useDetailViewModel();

  return (
    <Flex
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      mt={4}
    >
      <Stack
        w="100%"
        px="12px"
        spacing={5}
        alignItems="center"
        position="relative"
        overflow="hidden"
      >
        <Button
          p={1}
          minW="36px"
          h="36px"
          position="absolute"
          right="2rem"
          onClick={onDeletePokemon}
          _active={{
            bg: "primary.main",
          }}
        >
          <CloseIcon fontSize="12px" fontWeight="800" />
        </Button>
        <Stats
          data={dataPokemon}
          isLoading={isLoadEvolution}
          isReadyToEvolve={isReadyToEvolve}
          onEvolvePokemon={onHandlePokemonEvolveTo}
        />
        <Berry data={dataBerry} isLoading={isLoadBerry} />
        <ListBerries
          data={dataBerries}
          isLoading={isLoadBerries}
          selected={selectBerry}
          onSelect={handleSelectBerry}
        />
      </Stack>
      <Footer>
        <Button
          width="100%"
          mx="1rem"
          isDisabled={!selectBerry}
          onClick={onHandleFeedPokemon}
          _active={{
            bg: "primary.main",
          }}
        >
          Feed Pokemon
        </Button>
      </Footer>
    </Flex>
  );
};

export default DetailTemplate;
