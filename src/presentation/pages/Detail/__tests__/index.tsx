import { render } from "~/test";
import DetailTemplate from "..";

jest.mock("next/navigation", () => ({
  useRouter: () => ({
    pathname: "pikachu",
  }),
}));

describe("Detail Template component", () => {
  test("renders DetailTemplate component correctly", () => {
    const { queryByText } = render(<DetailTemplate />);
    expect(queryByText("Pikachu")).toBeDefined();
  });

  test("Detail test to match snapshot", () => {
    const { asFragment } = render(<DetailTemplate />);
    expect(asFragment()).toMatchSnapshot();
  });
});
