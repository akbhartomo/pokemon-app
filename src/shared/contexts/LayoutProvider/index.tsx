"use client";

import { useRouter } from "next/navigation";
import React, {
  ReactNode,
  createContext,
  useContext,
  useLayoutEffect,
  useMemo,
  useState,
} from "react";
import { IDetailPokemonModel } from "~/domain/model";
import localStorageService from "~/shared/utils/local-storage.adapter";

const LayoutContext = createContext({});

export const LayoutProvider: React.FC<{ children?: ReactNode }> = ({
  children,
}) => {
  const [isPrivate, setIsPrivate] = useState<boolean>(false);
  const router = useRouter();
  const data: IDetailPokemonModel | undefined = localStorageService(
    "pokemon",
    "secret"
  ).getAll();

  useLayoutEffect(() => {
    if (data?.name) {
      setIsPrivate(true);
    } else {
      setIsPrivate(false);
    }
  }, []);

  useLayoutEffect(() => {
    if (isPrivate) {
      router.push(`/pokemon/${data?.name}`);
    } else {
      router.push(`/`);
    }
  }, [isPrivate]);

  const value = useMemo(() => {
    return { isPrivate };
  }, [isPrivate]);

  return (
    <LayoutContext.Provider value={value}>{children}</LayoutContext.Provider>
  );
};

export const useLayoutContext = () => useContext(LayoutContext);

export default LayoutProvider;
