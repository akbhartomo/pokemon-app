import React from "react";
import { render } from "@testing-library/react";
import { useRouter } from "next/navigation";
import LayoutProvider from "..";

jest.mock("next/navigation", () => ({
  useRouter: jest.fn(),
}));

jest.mock("~/shared/utils/local-storage.adapter", () => ({
  __esModule: true,
  default: jest.fn(() => ({
    getAll: jest.fn(() => ({ name: "Pikachu" })), // Mocking a sample data
  })),
}));

describe("LayoutProvider component", () => {
  test("updates router based on isPrivate state", () => {
    const push = jest.fn();
    (useRouter as jest.Mock).mockReturnValue({ push });

    render(
      <LayoutProvider>
        <div>Test</div>
      </LayoutProvider>
    );

    expect(push).toHaveBeenCalledWith(`/pokemon/Pikachu`);
  });
});
