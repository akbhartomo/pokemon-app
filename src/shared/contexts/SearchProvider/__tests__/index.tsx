import React from "react";
import { render } from "~/test";
import SearchProvider, { useSearchContext } from "..";
import useSearchViewModel from "~/presentation/hooks/SearchContext/useSearchViewModel";

jest.mock("~/presentation/hooks/SearchContext/useSearchViewModel");

describe("SearchProvider component", () => {
  test("provides context value correctly", () => {
    (useSearchViewModel as jest.Mock).mockReturnValue({
      allPokemons: ["Pikachu", "Charmander"],
      loading: false,
      realTimeSearch: jest.fn(),
      searchResult: ["Pikachu"],
    });

    const TestComponent = () => {
      const contextValue = useSearchContext();
      return <div>{JSON.stringify(contextValue)}</div>;
    };

    const { container } = render(
      <SearchProvider>
        <TestComponent />
      </SearchProvider>
    );

    expect(container.textContent).toContain(
      '{"allPokemons":["Pikachu","Charmander"],"loading":false,"searchResult":["Pikachu"]}'
    );
  });
});
