import { reducer, initialState } from "../reducer";

describe("reducer function", () => {
  it("handles LOADING action correctly", () => {
    const action = { type: "LOADING" };
    const newState = reducer(initialState, action);
    expect(newState.loading).toBe(true);
  });

  it("handles GET_ALL_POKEMONS action correctly", () => {
    const mockPayload = ["Pikachu", "Bulbasaur"];
    const action = { type: "GET_ALL_POKEMONS", payload: mockPayload };
    const newState = reducer(initialState, action);
    expect(newState.loading).toBe(false);
    expect(newState.allPokemons).toEqual(mockPayload);
  });

  it("handles GET_SEARCH action correctly", () => {
    const mockPayload = ["Charmander", "Squirtle"];
    const action = { type: "GET_SEARCH", payload: mockPayload };
    const newState = reducer(initialState, action);
    expect(newState.loading).toBe(false);
    expect(newState.searchResult).toEqual(mockPayload);
  });

  it("returns the current state for unknown action types", () => {
    const action = { type: "UNKNOWN_ACTION" };
    const newState = reducer(initialState, action);
    expect(newState).toEqual(initialState);
  });
});
