import React from "react";

export interface ISearchResultItem {
  id: string;
  name: string;
  url: string;
}

export interface IInitStateSearchContext {
  loading?: boolean;
  searchResult: ISearchResultItem[];
  allPokemons: ISearchResultItem[];
}

export interface ISearchContext {
  children?: React.ReactNode;
}
