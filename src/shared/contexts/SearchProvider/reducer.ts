import type { IInitStateSearchContext } from "./interface";

const LOADING = "LOADING";
const GET_SEARCH = "GET_SEARCH";
const GET_ALL_POKEMONS = "GET_ALL_POKEMONS";

export const initialState: IInitStateSearchContext = {
  loading: false,
  searchResult: [],
  allPokemons: [],
};

export const reducer = (
  state: any,
  action: { type: string; payload?: any }
) => {
  switch (action.type) {
    case LOADING:
      return { ...state, loading: true };

    case GET_ALL_POKEMONS:
      return { ...state, allPokemons: action.payload, loading: false };

    case GET_SEARCH:
      return { ...state, searchResult: action.payload, loading: false };
  }

  return state;
};

export default reducer;
