"use client";

import React, { createContext, useContext, useMemo } from "react";
import type { ISearchContext } from "./interface";
import useSearchViewModel from "~/presentation/hooks/SearchContext/useSearchViewModel";

const SearchContext = createContext({});

export const SearchProvider: React.FC<ISearchContext> = ({ children }) => {
  const { allPokemons, loading, realTimeSearch, searchResult } =
    useSearchViewModel();

  const value = useMemo(() => {
    return {
      allPokemons,
      loading,
      searchResult,
      realTimeSearch,
    };
  }, [realTimeSearch]);

  return (
    <SearchContext.Provider value={value}>{children}</SearchContext.Provider>
  );
};

export const useSearchContext = () => {
  return useContext(SearchContext);
};

export default SearchProvider;
