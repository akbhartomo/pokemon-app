import axios, { AxiosResponse } from "axios";
import { CallApiParams } from "~/domain/model";
import { BASE_URL } from "../constants/base-url.constant";
import { logger } from "./logger";

export const callApi = async ({
  method,
  uri,
}: CallApiParams): Promise<AxiosResponse> => {
  const url = `${BASE_URL}/${uri}`;

  const defaultConfig = { method, url };
  const config = { ...defaultConfig };

  try {
    const response: AxiosResponse = await axios(config);
    return response;
  } catch (error: unknown) {
    let err;

    if (axios.isAxiosError(error) && error.response) {
      if (error.response?.status >= 400) {
        err = logger.error(error);
      }
      if (error.response?.status >= 500) {
        err = logger.info(error);
      }
    } else if (error instanceof Error) {
      err = logger.error(error);
    } else {
      throw new Error(`${error}`);
    }

    return err;
  }
};
