export function setLocalStorage(key: string, data: string) {
  localStorage.setItem(key, data);
}

export function getStoragePokemon(): string | undefined | null {
  if (typeof window !== "undefined") {
    const getPokemonStore = window.localStorage.getItem("pokemon");
    return getPokemonStore;
  }
}

export function removeLocalStorage(key: string) {
  localStorage.removeItem(key);
}
