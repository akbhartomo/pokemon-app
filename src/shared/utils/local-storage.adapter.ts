/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-unused-vars */

import CryptoJS from "crypto-js";

interface ITable {
  name: string;
}

function localStorageService<T extends ITable>(
  table: string,
  secretKey: string
) {
  const isBrowser = typeof window !== "undefined";

  if (!isBrowser) {
    return {
      getAll<T>() {
        return {} as T;
      },
      insert<T>(data: T) {},
      update<T>(name: string, data: Partial<T>) {},
      remove<T>() {},
    };
  }

  const encryptData = (data: any) =>
    CryptoJS.AES.encrypt(JSON.stringify(data), secretKey).toString();

  const decryptData = (encryptedData: string) => {
    const bytes = CryptoJS.AES.decrypt(encryptedData, secretKey);
    return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  };

  const setTable = (value: any) => {
    const encryptedValue = encryptData(value);
    window.localStorage.setItem(table, encryptedValue);
  };

  const getTable = () => {
    const encryptedValue = window.localStorage.getItem(table);
    if (encryptedValue) {
      return decryptData(encryptedValue);
    }
    return null;
  };

  const removeTable = (name: string) => window.localStorage.removeItem(name);

  if (!getTable()) {
    setTable({});
  }

  const allLocal = () => getTable() || {};

  return {
    getAll<T>() {
      return allLocal() as T;
    },

    insert<T>(data: T) {
      let existingData = allLocal() as T;
      existingData = data;
      setTable(existingData);
    },

    update<T>(name: string, data: Partial<T>) {
      let existingData = allLocal() as T;

      if (name) {
        existingData = { ...existingData, ...data };
        setTable(existingData);
      }
    },

    remove<T>(name: string) {
      removeTable(name);
    },
  };
}

export default localStorageService;
