import { renderHook, act } from "~/test";
import { useDebounce } from "../debounce";

describe("useDebounce", () => {
  beforeEach(() => {
    jest.useFakeTimers(); // Mock timers
  });

  afterEach(() => {
    jest.runOnlyPendingTimers(); // Run only pending timers after each test
    jest.useRealTimers(); // Restore real timers
  });

  it("returns the debounced value after the delay", () => {
    const initialValue = "hello";
    const initialDelay = 500;

    const { result, rerender } = renderHook(
      ({ value, delay }) => useDebounce(value, delay),
      { initialProps: { value: initialValue, delay: initialDelay } }
    );

    // Value should be the same as initial value
    expect(result.current).toBe(initialValue);

    // Update value
    const updatedValue = "world";
    act(() => {
      rerender({ value: updatedValue, delay: initialDelay });
    });

    // Value should still be the same as initial value before delay
    expect(result.current).toBe(initialValue);

    // Fast-forward time by initial delay
    act(() => {
      jest.advanceTimersByTime(initialDelay);
    });

    // Value should now be the updated value after the delay
    expect(result.current).toBe(updatedValue);
  });

  it("returns the same value when value is undefined", () => {
    const { result } = renderHook(() => useDebounce(undefined));

    expect(result.current).toBeUndefined();
  });

  it("returns the same value with custom delay", () => {
    const { result } = renderHook(() => useDebounce("hello", 1000));

    expect(result.current).toBe("hello");
  });
});
