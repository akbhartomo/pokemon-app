import { ensureNonNegative } from "../non-negative";

describe("ensureNonNegative function", () => {
  it("returns the same positive number when input is positive", () => {
    const positiveNumber = 5;
    expect(ensureNonNegative(positiveNumber)).toBe(positiveNumber);
  });

  it("returns zero when input is zero", () => {
    expect(ensureNonNegative(0)).toBe(0);
  });

  it("returns zero when input is negative", () => {
    const negativeNumber = -5;
    expect(ensureNonNegative(negativeNumber)).toBe(0);
  });

  it("returns zero when input is NaN", () => {
    const nanValue = NaN;
    expect(ensureNonNegative(nanValue)).toBe(0);
  });
});
