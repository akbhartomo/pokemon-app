import { textToUpperCase } from "../string";

describe("textToUpperCase function", () => {
  it("converts text to uppercase", () => {
    const inputText = "hello";
    const expectedText = "HELLO";
    const result = textToUpperCase(inputText);
    expect(result).toEqual(expectedText);
  });

  it("handles empty string", () => {
    const inputText = "";
    const expectedText = "";
    const result = textToUpperCase(inputText);
    expect(result).toEqual(expectedText);
  });

  it("handles special characters", () => {
    const inputText = "!@#$%^&*()";
    const expectedText = "!@#$%^&*()";
    const result = textToUpperCase(inputText);
    expect(result).toEqual(expectedText);
  });

  it("handles mixed case text", () => {
    const inputText = "HeLLo";
    const expectedText = "HELLO";
    const result = textToUpperCase(inputText);
    expect(result).toEqual(expectedText);
  });
});
