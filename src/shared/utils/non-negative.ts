export function ensureNonNegative(value: number): number {
  return value >= 0 ? value : 0;
}
