export const fonts = {
  heading: "'Manrope', '-apple-system', 'BlinkMacSystemFont', sans-serif",
  body: "'Manrope', '-apple-system', 'BlinkMacSystemFont', sans-serif",
  mono: "'Manrope', '-apple-system', 'BlinkMacSystemFont', sans-serif",
};
