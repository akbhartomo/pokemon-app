import { ComponentStyleConfig } from "@chakra-ui/react";

export const baseStyleButton: ComponentStyleConfig = {
  baseStyle: {
    borderRadius: "10rem",
    overflow: "hidden",
    textAlign: "center",
    fontWeight: "800",
  },
  variants: {
    solid: {
      bg: "primary.dark",
      color: "white",
    },
  },
  defaultProps: {
    colorScheme: "primary.dark",
    size: "lg",
    variant: "solid",
  },
};
