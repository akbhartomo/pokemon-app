import { globals } from "../globals";

describe("Globals object", () => {
  test("has the correct structure and values", () => {
    expect(globals).toEqual({
      "html,body": {
        margin: 0,
        padding: 0,
        color: "general.dark",
        background: "background.primary",
        fontFamily:
          "'Manrope', '-apple-system', 'BlinkMacSystemFont', sans-serif",
      },
      a: {
        textDecoration: "none",
      },
      ul: {
        margin: 0,
        padding: 0,
        listStyle: "none",
      },
      input: {
        background: "white",
      },
      "::placeholder": {
        color: "general.light",
      },
      "main::-webkit-scrollbar": {
        width: "2px",
        height: "2px",
        borderRadius: "10px",
      },
      "main::-webkit-scrollbar-track": {
        boxShadow: "inset 0 0 0 rgba(0, 0, 0, 0.3)",
      },
      "main::-webkit-scrollbar-thumb": {
        backgroundColor: "#9b1142",
      },
    });
  });
});
