import { fonts } from "../fonts";

describe("Fonts object", () => {
  test("has the correct structure and values", () => {
    expect(fonts).toEqual({
      heading: "'Manrope', '-apple-system', 'BlinkMacSystemFont', sans-serif",
      body: "'Manrope', '-apple-system', 'BlinkMacSystemFont', sans-serif",
      mono: "'Manrope', '-apple-system', 'BlinkMacSystemFont', sans-serif",
    });
  });
});
