import { ComponentStyleConfig } from "@chakra-ui/react";
import { baseStyleButton } from "../button"; // Assuming your file is named "style.ts"

describe("baseStyleButton", () => {
  test("should match the expected structure", () => {
    const expected: ComponentStyleConfig = {
      baseStyle: {
        borderRadius: "10rem",
        overflow: "hidden",
        textAlign: "center",
        fontWeight: "800",
      },
      variants: {
        solid: {
          bg: "primary.dark",
          color: "white",
        },
      },
      defaultProps: {
        colorScheme: "primary.dark",
        size: "lg",
        variant: "solid",
      },
    };

    expect(baseStyleButton).toEqual(expected);
  });
});
