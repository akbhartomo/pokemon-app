import { colorMode } from "../color-mode";

describe("colorMode object", () => {
  it("should have initialColorMode set to 'light'", () => {
    expect(colorMode.initialColorMode).toEqual("light");
  });

  it("should have useSystemColorMode set to false", () => {
    expect(colorMode.useSystemColorMode).toEqual(false);
  });
});
