export const palette = {
  primary: {
    main: "#ae1b52",
    dark: "#9b1142",
  },
  general: {
    main: "#00020f",
    dark: "#2d2926",
    light: "#bdbfd5",
  },
  blue: {
    main: "#0f6389",
    soft: "#17809f",
  },
  background: {
    primary: "#ffe69d",
  },
  surface: "#eeeef0",
  warning: {
    main: "#f4dd38",
    mid: "#d5bd25",
  },
  border: "#bdbfd5",
  black: "#000",
  white: "#fff",
};
