import { extendTheme } from "@chakra-ui/react";
import { baseStyleButton } from "./button";
import { palette } from "./color";
import { fonts } from "./fonts";
import { globals } from "./globals";
import { colorMode } from "./color-mode";

const createTheme = extendTheme({
  config: { ...colorMode },
  components: {
    Button: baseStyleButton,
  },
  colors: palette,
  fonts: fonts,
  styles: {
    global: () => ({
      ...globals,
    }),
  },
});

export { createTheme };
