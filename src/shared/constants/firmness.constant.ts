export const VERY_SOFT = "very-soft";
export const SOFT = "soft";
export const HARD = "hard";
export const VERY_HARD = "very-hard";
export const SUPER_HARD = "super-hard";
