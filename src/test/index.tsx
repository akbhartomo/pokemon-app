import { ReactElement } from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import {
  render as baseRender,
  RenderOptions,
  RenderResult,
} from "@testing-library/react";

interface IAllProvidersProps {
  children: React.ReactNode;
}

export const AllProviders: React.FC<IAllProvidersProps> = ({ children }) => {
  const client = new QueryClient();
  return <QueryClientProvider client={client}>{children}</QueryClientProvider>;
};

const render = (ui: ReactElement, options?: Omit<RenderOptions, "queries">) =>
  baseRender(ui, { wrapper: AllProviders, ...options }) as RenderResult;

export * from "@testing-library/react";

export { render };
